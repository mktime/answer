//center

function center(obj, style) {
	var screenWidth = $(window).width(),
		screenHeight = $(window).height();
	var scrolltop = $(document).scrollTop();
	var objLeft = (screenWidth - obj.width()) / 2;
	var objTop = (screenHeight - obj.height()) / 2 + scrolltop;

	obj.css({
		position: 'absolute',
		left: objLeft + 'px',
		//top: objTop + 'px' /*,'display': 'block'*/
	    top: 8+"%"
	});

	$(window).bind('resize', function() {
		center(obj, !style);
	});

	if (true == style) {
		$(window).bind('scroll', function() {
			objTop = (screenHeight - obj.height()) / 2 + $(document).scrollTop();
			obj.css({
				top: objTop + 'px'
			});
		});
	} else {
		$(window).unbind('scroll');
	}
}

$.fn.extend({
	//dialog.start
	jumpBox: function(style) {
		var _method = $(this);
		$(this).css({
			'position': 'absolute'
		});
		//$("body select").css('visibility', 'hidden');
		$("body .popWrap select").css('visibility', 'visible');
		center($(this), style);
		$("#screen").css({
			'display': 'block',
			'width': '100%',
			'height': $(document).height()
		});
		$(this).css({
			'display': 'block'
		});
		$(this).find("*[name='close']").click(function() {
			$("body select").css('visibility', 'visible');
			$("#screen").hide();
			_method.hide();
			return false;
		})
	},
	//dialog.over 
	
});

